PACKAGE:=vendor/node_modules.tar.gz
NODE_VERSION:=$(shell cat Dockerfile | head -n 1 | cut -d ' ' -f 2)
ENVIRONMENT:=tmp/environment
NODE_MODULES:=tmp/node_modules

.PHONY: check
check:
	docker --version > /dev/null
	docker-compose --version > /dev/null

.PHONY: foo
$(PACKAGE): package.json
	mkdir -p $(@D)
	docker create -i --name installer -w /data $(NODE_VERSION) npm install
	docker cp package.json installer:data/package.json
	docker start -ai installer
	docker cp installer:/data/node_modules - | gzip > $@
	docker stop installer
	docker rm installer

$(NODE_MODULES): $(PACKAGE)
	tar zxf $<
	mkdir -p $(@D)
	touch $@

$(ENVIRONMENT): $(NODE_MODULES) docker-compose.yml
	docker-compose build
	docker-compose up -d app
	mkdir -p $(@D)
	touch $@

.PHONY: test
test: $(ENVIRONMENT)
	docker-compose run --rm tests npm test

.PHONY: test-image
test-image: $(ENVIRONMENT)
	docker-compose run --rm app npm test
	docker-compose run --rm app npm run lint

.PHONY: test-smoke
test-smoke: $(ENVIRONMENT)
	docker-compose run --rm smoke

.PHONY: test-ci
test-ci: test-smoke test-image

.PHONY: clean
clean:
	-@docker stop installer > /dev/null 2> /dev/null
	-@docker rm installer > /dev/null 2> /dev/null
	docker-compose down
	rm -rf $(ENVIRONMENT) $(NODE_MODULES) node_modules
